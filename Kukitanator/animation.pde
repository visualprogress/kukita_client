int      MIN_FRAMES_WITH_FACE = 30;
boolean  withFace;
int      framesWithFace=0;
float    Xcl,Ycl;

boolean    oneAdded;
Rectangle[]  positions;

PFont font;

void initAnimation(){
  font = loadFont("TerminatorRealNFI-48.vlw");
  println(font);
  
  
}

void drawAnimation(){
  tint(255,0,0);
  
  textFont(font,16);
  fill(255,255,255);
  text(nf(random(0,1000000000),10,0),10,20);
  text(nf(random(0,1000000000),10,0),10,40);
  
  
  if(Xc==0.0 && Yc==0.0){
    withFace = false;   
  }
  else{
    noFill();
    stroke(140);
    strokeWeight(1);
    for (int i = 0; i < faces.length; i++) {
      rect(faces[i].x* ratioImage + xCoord,faces[i].y* ratioImage + yCoord,faces[i].width* ratioImage,faces[i].height* ratioImage);
    }
      
    stroke(200);
    strokeWeight(3);
    rect(Xc, Yc, Wc, Hc);
    
   
    withFace = true;  
  }
  
  if(withFace){
    if(MIN_FRAMES_WITH_FACE < framesWithFace){
      
    } 
    else{
      
        textFont(font,16);
        fill(255,255,255);
        text("SCANNING",Xc+5,Yc+15);
  
       framesWithFace++;
    } 
    
    textFont(font,14);
    fill(255,255,255);
    text("FACES DETECTED",displayWidth-230,20);
    text("-----------------------",displayWidth-230,30);
    for (int i = 0; i < faces.length; i++) {
      text(faces[i].x+","+faces[i].y+" "+faces[i].width+"x"+faces[i].height,displayWidth-230,20*i+50);
    }
    
    
  }
  else{
    framesWithFace=0;
  }
  
  //title
  textFont(font,16);
  fill(255,255,255);
  text("EXPO STARK",20,displayHeight-140);
  textFont(font,14);
  text("SANTIAGO - CHILE",20,displayHeight-120);
  text("powered by VISUALPROGRESS",20,displayHeight-100);
  
  //pos
  //ShowPosGrid(displayWidth-290,displayHeight-250,250,150);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) {
       
    } else if (keyCode == RIGHT) {
      
    } 
  }
}


void ShowPosGrid(int x,int y,int w, int h){
  int step = 10;
  stroke(220);
  strokeWeight(1);
  noFill();
  rect(x,y,w,h);
  
  for(int X=x+step;X<x+w;X+=step){
     line(X,y,X+w,y); 
  }
  
 
  
  
}

  
