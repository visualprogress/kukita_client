/************************************************************************
* File Name          : uArm_Client
* Author             : Evan
* Updated            : Evan
* Version            : V0.5
* Date               : 2 Feb, 2015
* Description        : Processing to control uArm using LeapMotion
                       using the "Standard.ino" on uArm.
                       Need G4P library and LeapMotionForProcessing library.
* License            : 
* Copyright(C) 2014 UFactory Team. All right reserved.
*************************************************************************/

import g4p_controls.*;
import processing.serial.*;
import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import javax.swing.*;


boolean CAMERA_EN  = true;
boolean UPDATE_EN = false;
boolean SERIAL_EN = true;
boolean SOCKET_EN = false;

boolean TRACKING_EN = true;
boolean ANIMATION_EN = true;

DatagramClient client = new DatagramClient();
String ip = "0.0.0.0";
int port = 0;

byte suctionCup = 0;

int wVideo = 320;
int hVideo = 240;
float ratioVideo = (float)wVideo/(float)hVideo;
int xCoord = 0;
int yCoord = 0;
int wImage = 0;
int hImage = 0;
float ratioImage = 0;
float Xc,Yc,Wc,Hc;
float Xr,Yr;
Rectangle[] faces;

public void setup()
{
  //size(995, 450, JAVA2D);
  size(displayWidth, displayHeight, JAVA2D);
  
  wImage     = displayWidth - (xCoord*2);
  hImage     = (int)(wImage / ratioVideo);
  ratioImage = (float) wImage / (float) wVideo;

  createGUI();
  customGUI();
  scanPort();
  
  if(CAMERA_EN)
  {
    initCamera();
  }
  
  if(ANIMATION_EN){
    initAnimation();
  }
}

public void draw()
{
  background(220);
  
  if(CAMERA_EN)
  {
    readCamera();
  }
  else{
    stroke(0, 0, 0);
    strokeWeight(1);
    rect(xCoord,yCoord,wImage,hImage);
  }
  
  if(UPDATE_EN)
  {
    sendPos(slider2d2.getValueYI(), slider1.getValueI(), slider2d2.getValueXI(), 0, suctionCup);
    UPDATE_EN = false;
  }
  
  if(ANIMATION_EN){
    drawAnimation();
  }
}

void setUIPos(int _handPosX, int _handPosY, int _handPosZ, int _handRot)
{
  slider1.setValue(_handPosY);
  slider2d2.setValueX(_handPosX);
  slider2d2.setValueY(_handPosZ);
  
  Xr = _handPosX;
  Yr = _handPosY;
}

// Use this method to add additional statements
// to customise the GUI controls
public void customGUI()
{
  slider2d2.setEasing(8.0);
  frame.setTitle("Kukitaneitor");
}


//taken from http://processing.org/learning/topics/directorylist.html
File[] listFiles(String dir) {
 File file = new File(dir);
 if (file.isDirectory()) {
   File[] files = file.listFiles();
   return files;
 } else {
   // If it's not a directory
   return null;
 }
}
