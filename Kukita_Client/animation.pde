
int      MAX_FRAMES_WITHOUT_FACE = 20;
int      MAX_ICON_FLYING = 50;
int      Y_STEP = 3;
int      W_STEP = 3;
int      H_STEP = 3;
PImage[] icons;
int      icon;
int      iconsCount;
boolean  withFace;
int      framesWithoutFace;
float    Xcl,Ycl;

boolean    oneAdded;
Rectangle[]  positions;

void initAnimation(){
  String path = sketchPath+"/icons/"; 
  File[] files = listFiles(path);

  print(path+"\n"); 
  print(files.length+"\n"); //how many files are here
  println();

  icons = new PImage[files.length];
  int j=0;
  for(int i=0;i<files.length;i++) {
    if(files[i].getAbsolutePath().endsWith(".png")){ 
      println("cargando "+files[i]); 
      icons[j]=loadImage(files[i].getAbsolutePath()); 
      if(icons[j]!=null){ 
        println("cargado!");
      }
      j++;
    }
  }
  iconsCount = j;
  icon = 0;
  withFace = false;
  framesWithoutFace = 0;
  
  positions = new Rectangle[MAX_ICON_FLYING];
  for(int i=0;i<MAX_ICON_FLYING;i++){
     positions[i] = new Rectangle();
     positions[i].x = 0;
     positions[i].y = 0;
  }
}

void drawAnimation(){
  //actualizar posiciones
  if(Xc==0.0 && Yc==0.0){
    if(withFace){
       framesWithoutFace = 0;
    }
    framesWithoutFace++;
    withFace = false;   
  }
  else{
    if(MAX_FRAMES_WITHOUT_FACE < framesWithoutFace){
       //icon = (int) random(0,iconsCount-1);
       icon++;
       if(iconsCount <= icon){
         icon = 0;
       }
    }
   
    for(int i=0;i<MAX_ICON_FLYING;i++){
      if(positions[i].x==0 && positions[i].y==0){
        positions[i].x      = (int)(Xc + random(-Wc/2,Wc/2));
        positions[i].y      = (int)(Yc - random(Hc*0.75,Hc*1.2));
        positions[i].width  = (int)(random(Wc*0.3,Wc*0.4));
        positions[i].height = (int)(positions[i].width *(icons[icon].height/icons[icon].width));
        break;
      }
    } 
    
    framesWithoutFace = 0;
    withFace = true;  
  }
  
  //actualizar iconos
  for(int i=0;i<MAX_ICON_FLYING;i++){
      if(positions[i].x!=0 && positions[i].y!=0){
          positions[i].y -= Y_STEP;
          positions[i].width  -= W_STEP;
          positions[i].height -= H_STEP;
          
          if(positions[i].y <= 0 || positions[i].width <= 0 || positions[i].height <= 0){
              positions[i].x = 0;
              positions[i].y = 0;
          }
          else{
            image(icons[icon],positions[i].x,positions[i].y,positions[i].width,positions[i].height);
          }
      }
    }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) {
       icon--;
       if(icon < 0){
         icon = iconsCount - 1;
       }
    } else if (keyCode == RIGHT) {
       icon++;
       if(iconsCount <= icon){
         icon = 0;
       }
    } 
  }
}

  
