// Camera

Capture video;
OpenCV opencv;
GWindow[] cameraWindow;

float x,y,w,h;
int margin = 30;
int margin2 = 80;
float factorAjusteX = 0.2;
float factorAjusteY = 0.4;

float w_2Video = wVideo/2;
float h_2Video = hVideo/2;

float xs,ys,ws;

void initCamera()
{
  String[] cameras = Capture.list();
  if (cameras.length == 0) {
    println("No hay camaras");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(i+": "+cameras[i]);
    }
  } 
  
  video = new Capture(this, wVideo, hVideo);//,"USB Camera");
  opencv = new OpenCV(this, wVideo, hVideo);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
  video.start();
}

void exitCamera()
{
  noTint(); // Disable tint
  video.stop();
}

void readCamera()
{
 // scale(2);
  opencv.loadImage(video);
  //tint(255, 200);  // Display at half opacity:
  image(video, xCoord, yCoord,wImage,hImage);
  noFill();
  
  if(TRACKING_EN){
    Rectangle[] faces = opencv.detect();
    if(0<faces.length){
      x = 0.0;
      y = 0.0;
      w = 0.0;
      h = 0.0;
      
      //_____________________________________
      // Centro entre todas las caras 
      /*
      stroke(0,0, 255);
      strokeWeight(3);
      for (int i = 0; i < faces.length; i++) {
        xc = faces[i].x + faces[i].width/2;
        yc = faces[i].y + faces[i].height/2;
   
        rect(faces[i].x * ratioImage + xCoord, faces[i].y * ratioImage + yCoord, faces[i].width * ratioImage, faces[i].height * ratioImage, 6);
        x+= xc ;
        y+= yc;
        w+= faces[i].width;
      }
      x /= faces.length;
      y /= faces.length;
      w /= faces.length;
      //_____________________________________
      */
      
      
      //_____________________________________
      // Cara más cercana (grande) a la cámara 
      for (int i = 0; i < faces.length; i++) {
        if(w < faces[i].width){
          x = faces[i].x + faces[i].width/2;
          y = faces[i].y + faces[i].height/2;
          w = faces[i].width;
          h = faces[i].height;
        }
      }
      
      
      if(0.0 < x && 0.0 < y){  
        //Calcular posicion en pantalla
        Xc = x * ratioImage + xCoord;
        Yc = y * ratioImage + yCoord;
        Wc = w * ratioImage;
        Hc = h * ratioImage;        
        //ellipse(Xc,Yc,10,10);

        //_____________________________________
        //mover robot para encuadrar cara
        float curXPos = slider2d2.getValueXI();
        float curYPos = slider1.getValueI();
        float curZPos = slider2d2.getValueYI();
        
        if(x < w_2Video - margin){
            curXPos -= (w_2Video-x)*factorAjusteX;
            /*
            if(curXPos < -90){
              curXPos = -90;
            }
            */
            if(curXPos < -40){
              curXPos = -40;
            }
            
        }
        else if (w_2Video + margin < x){
            curXPos += (x - w_2Video)*factorAjusteX;
            /*
            if(90 <curXPos){
              curXPos = 90;
            }
            */
            if(40 <curXPos){
              curXPos = 40;
            }
            
        }
        
        if(y < h_2Video - margin){
            curYPos += (h_2Video - y)*factorAjusteY;
            
            
            if(180<curYPos){
              curYPos = 180;
            }
        }
        else if (h_2Video + margin < y){
            curYPos -= (y - h_2Video)*factorAjusteY;
            /*
            if(curYPos < -180){
              curYPos = -180;
            }
            */
            if(curYPos < 0){
              curYPos = 0;
            }
        }
         
        setUIPos((int)curXPos,(int)curYPos,(int)curZPos,0);
      }
    }
    else{
      Xc = 0.0;
      Yc = 0.0;
    }
  }
}

void captureEvent(Capture c)
{
  c.read();
}



